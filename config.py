# Copyright 2018 github.com/aski6
# 
# This file is part of https://gitlab.com/theexperiencedameteurs/airport-robot-controller.
# 
# https://gitlab.com/theexperiencedameteurs/airport-robot-controller is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# https://gitlab.com/theexperiencedameteurs/airport-robot-controller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with https://gitlab.com/theexperiencedameteurs/airport-robot-controller.  If not, see <https://www.gnu.org/licenses/>.



# Robot ip addresses, ports, etc. (location on the internet/network).
robot_address = "172.23.167.41" 
robot_port = 5000
robot_address_port = robot_address + ":" + str(robot_port)
 
#page urls for each comms command location on the robot web server.
robot_page_goto = "http://" + robot_address_port + "/goto/"
robot_page_rev = "http://" + robot_address_port + "/rev/"
robot_page_status = "http://" + robot_address_port + "/status/"

#database ip addresses, type, etc. (values needed to talk to it).
database_address = "192.168.122.84"
database_user = "theexperiencedameteurs"
database_password = "prewired2018"
database_name = "airport"
