import config
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#get a connection to the database to communicate with stored data and save new data
connection = create_engine("mysql+pymysql://" + config.database_user + ":" + config.database_password + "@" + config.database_address + ":3306/" + config.database_name)

#get information about the database we are using
Base = declarative_base()

class User(Base):
	__tablename__ = "users"
	id = Column(Integer, primary_key = True, autoincrement = True)
	first_name = Column(String(10))
	second_name = Column(String(10))

class Command(Base):
	__tablename__ = "commands"
	id = Column(Integer, primary_key = True, autoincrement = True)
	user_id = Column(Integer, ForeignKey(User.id))
	request_text = Column(String(255))

class Flight(Base):
        __tablename__ = "flights"
        id = Column(Integer, primary_key = True, autoincrement = True)
        user_id = Column(Integer, ForeignKey(User.id))
        origin = Column(String(20))
        destination = Column(String(20))
        time = Column(String(20))

Base.metadata.create_all(connection)

Session = sessionmaker(bind=connection)
session = Session()

