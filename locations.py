# Copyright 2018 github.com/aski6
# 
# This file is part of https://gitlab.com/theexperiencedameteurs/airport-robot-controller.
# 
# https://gitlab.com/theexperiencedameteurs/airport-robot-controller is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# https://gitlab.com/theexperiencedameteurs/airport-robot-controller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with https://gitlab.com/theexperiencedameteurs/airport-robot-controller.  If not, see <https://www.gnu.org/licenses/>.


#direction string format: nd
#where n is number of 'steps' (times colored line hit to indicate turn), and d is direction to turn (l/r).

# array of direction strings for each location from the 'home' starting location.
#this variable definition is provided by another copyright holder. Permission is given for use and distribution, but copyright was released.
directions_home = {
	"bar-burrito":"1r",
	"wh-smiths":"2r",
	"yo-sushi":"3r",
	"gate-1":"1l",
	"gate-2":"2l",
	"gate-3":"3l",
}

# array of direction strings to return to home from each location. TODO enter directions & locations.
directions_home_back = {
	"1":"1l",
	"2":"1r",
	"3":"2l",
	"4":"2r",
	"5":"3l",
	"6":"3r",
}

