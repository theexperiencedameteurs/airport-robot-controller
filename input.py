# Copyright 2018 github.com/aski6
# 
# This file is part of https://gitlab.com/theexperiencedameteurs/airport-robot-controller.
# 
# https://gitlab.com/theexperiencedameteurs/airport-robot-controller is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# https://gitlab.com/theexperiencedameteurs/airport-robot-controller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with https://gitlab.com/theexperiencedameteurs/airport-robot-controller.  If not, see <https://www.gnu.org/licenses/>.

import requests
import config
import database
from locations import *
from messages import *

running = True
in_conversation = False
conversation_step = 0
current_user = ""

# process a text command from a user into arguments to send to the appropriate action function. Store command sent by user in database.
def interp_command(command):
	global running
	global current_user
	global conversation_step
	#get all words in command as array, words split with spaces.
	args = command.split(" ")
	if (conversation_step == 1):
		current_user = " ".join(args)	
		in_conversation = True
	else:
		db_current_userid = database.session.query(database.User).filter_by(first_name = current_user.split(" ")[0], second_name = current_user.split(" ")[1]).first().id
		if (args[0] == 'exit'):
			running = False
		elif (args[0] == "show" or args[0] == "take" or args[0] == ""):
			#TODO take location from command if some natural language is expected.
			print("no problem, just follow me")
			conversation_step = 2
			lead_user(args[1])
		elif (args[0] == "what" or args[0] == "time" or args[0] == "flight"):
			time, destination = get_departure_time(db_current_userid)
			print("Your departure time for {} is {}.".format(destination, time))
			conversation_step = 2
		user_request = database.Command(user_id=db_current_userid, request_text=command)
		database.session.add(user_request)
		database.session.commit()

# output messages appropriate to the status of commanding the robot to make commanding more like a conversation.
def output_conversation_step_message(step):
	message = message_text.get(step)
	if (step == 1):
		print(message)
	elif (step == 2):
		print(message.format(current_user))
	elif (step > 2):
		print(message)

# functions to perform each feature of the robot.

def lead_user(location):
	#get the directions string for the location requested.
	directions = directions_home.get(location)
	#send a move command to the robot with the found directions.
	send_forward_command(directions)

def return_from(location):
	directions = directions_home_back.get(location)
	send_reverse_command()

#retreive the departure time and destination from the database for the user requesting.
def get_departure_time(request_user_id):
        flight_info = database.session.query(database.Flight).filter_by(user_id=request_user_id).first()
        return flight_info.time, flight_info.destination



#functions for sending http commands to each http location on robot for each purpose.

# send http command for robot to move forward according to a direction string (format defined with a comment in "locations.py").
def send_forward_command(directions):
	response = requests.get(config.robot_page_goto, {"l":directions})
	return response.status_code

# send http command for robot to move backwards according to the direction string for reversing from the location (format defined with a comment in "locations.py").
def send_reverse_command(directions):
	response = requests.get(config.robot_page_rev, {"l":directions})
	return response.status_code

# get the status of a variable key in the robot's status page.
def poll_status(key):
	response = requests.get(config.robot_page_status)
	#TODO search status page response for the data corresponding to the requested key.



# run an interactive cli if this script is started manually and not imported.
if (__name__ == "__main__"):
	while (running == True):
		conversation_step += 1
		output_conversation_step_message(conversation_step)
		request = input()
		interp_command(request)

